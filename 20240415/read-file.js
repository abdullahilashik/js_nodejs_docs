const {read} = 'node:fs';

try {
    const data = read('../data/names.txt');
    console.log(data);
}catch(error){
    console.log(error.message);
}